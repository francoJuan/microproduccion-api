import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import 'reflect-metadata';
import { createConnection } from 'typeorm';

import unidadMedidaRoutes from './routes/unidadMedida.route';

//-----------CONFIG----------------//
const app = express();
createConnection();

//-----------MIDDELWARES------------//
app.use(cors());
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));

//-------------ROUTES--------------//
app.use(unidadMedidaRoutes);

app.set('port', process.env.PORT || 3000);

app.listen(app.get('port'));
console.log('server on port ' + app.get('port'));