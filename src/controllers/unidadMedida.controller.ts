import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { UnidadMedida } from "../entities/unidadMedida";

export const getUnidadMedidas = async (req: Request, res: Response): Promise<Response> => {
    const unidadMedidas =  await getRepository(UnidadMedida).find();
    return res.json(unidadMedidas);
}

export const getUnidadMedida = async (req: Request, res: Response): Promise<Response> => {
    const codigo = req.params.codigo;
    const unidadMedida =  await getRepository(UnidadMedida).findOne(codigo);
    return res.json(unidadMedida);
}

export const addUnidadMedida = async (req: Request, res: Response): Promise<Response> => {
    const newUnidadMedida = getRepository(UnidadMedida).create(req.body);
    const result = await getRepository(UnidadMedida).save(newUnidadMedida);
    return res.json(result);
}

export const updateUnidadMedida = async (req: Request, res: Response): Promise<Response> => {
    const codigo = req.params.codigo;
    const unidadMedida =  await getRepository(UnidadMedida).findOne(codigo);
    
    if(unidadMedida){
        getRepository(UnidadMedida).merge(unidadMedida, req.body);
        const result = await getRepository(UnidadMedida).save(unidadMedida);
        return res.json(result);
    }

    return res.json({msg: "Error al actualizar"});
}

export const deleteUnidadMedida = async (req: Request, res: Response): Promise<Response> => {
    const codigo = req.params.codigo;
    const unidadMedida =  await getRepository(UnidadMedida).delete(codigo);
    return res.json(unidadMedida);
}