import { Router } from "express";
import { addUnidadMedida, getUnidadMedida, getUnidadMedidas, updateUnidadMedida, deleteUnidadMedida } from "../controllers/unidadMedida.controller";

const router = Router();

router.get('/unidadMedida', getUnidadMedidas );
router.post('/unidadMedida', addUnidadMedida );
router.get('/unidadMedida/:codigo', getUnidadMedida);
router.put('/unidadMedida/:codigo', updateUnidadMedida );
router.delete('/unidadMedida/:codigo', deleteUnidadMedida );

export = router;