import { Entity, Column, PrimaryGeneratedColumn, } from 'typeorm';

@Entity()
export class UnidadMedida {

    @PrimaryGeneratedColumn({name: "idUnidadMedida"})
    codigo: number;

    @Column()
    unidad: String;
    
    @Column()
    simbolo: String;
}