"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var morgan_1 = __importDefault(require("morgan"));
var cors_1 = __importDefault(require("cors"));
require("reflect-metadata");
var typeorm_1 = require("typeorm");
var unidadMedida_route_1 = __importDefault(require("./routes/unidadMedida.route"));
//-----------CONFIG----------------//
var app = express_1.default();
typeorm_1.createConnection();
//-----------MIDDELWARES------------//
app.use(cors_1.default());
app.use(morgan_1.default('dev'));
app.use(express_1.default.json());
app.use(express_1.default.urlencoded({ extended: false }));
//-------------ROUTES--------------//
app.use(unidadMedida_route_1.default);
app.set('port', process.env.PORT || 3000);
app.listen(app.get('port'));
console.log('server on port ' + app.get('port'));
